import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import { AppComponent } from './app.component';
import { AngularFireModule } from '@angular/fire';
import { AngularFireDatabaseModule } from '@angular/fire/database';
import { AngularFireAuthModule } from '@angular/fire/auth';
import { FormsModule } from '@angular/forms';
import { Routes,RouterModule } from '@angular/router';
import {environment} from '../environments/environment';
import { WelcomeComponent } from './welcome/welcome.component';
import { SignupComponent } from './signup/signup.component';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatInputModule} from '@angular/material';
import {MatButtonModule} from '@angular/material/button';
import { LoginComponent } from './login/login.component';
import { NavComponent } from './nav/nav.component';
import {MatCardModule} from '@angular/material/card';
import { TodoComponent } from './todo/todo.component';
import {MatCheckboxModule} from '@angular/material/checkbox';
import {MatSelectModule} from '@angular/material/select';
@NgModule({
  declarations: [
    AppComponent,
    WelcomeComponent,
    SignupComponent,
    LoginComponent,
    NavComponent,
    TodoComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    FormsModule,  
    MatInputModule,
    MatFormFieldModule,
    MatButtonModule,
    MatCardModule,
    MatCheckboxModule,
    MatSelectModule,
    AngularFireModule.initializeApp(environment.firebase),
    AngularFireDatabaseModule,
    AngularFireAuthModule,
   RouterModule.forRoot([
     {path:'signup', component:SignupComponent},
     {path:'login', component:LoginComponent},
     {path:'welcome', component:WelcomeComponent},
     {path: 'welcome/:id', component: WelcomeComponent },
   //  {path:'register', component:RegisterComponent},
   //  {path:'login', component:LoginComponent},
   //  {path:'codes', component:CodesComponent},
     {path:'**', component:SignupComponent}
   ])
 ],

  
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
