import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from '../auth.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
email = '';
password = '';

googleLogin(){
this.auth.loginGoogle().then(value => {
this.router.navigate(['/welcome'])})
}

login(){
this.auth.login(this.email,this.password).then(value => {console.log(value)})
}

  constructor(private router:Router,private auth:AuthService) { }

  ngOnInit() {
  }

}
