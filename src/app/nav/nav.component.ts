import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from '../auth.service';

@Component({
  selector: 'app-nav',
  templateUrl: './nav.component.html',
  styleUrls: ['./nav.component.css']
})
export class NavComponent implements OnInit {
toLogin(){
  return this.router.navigate(['/login']);
}
toWelcome(){
  return this.router.navigate(['/welcome']);
}
toRegister(){
  return this.router.navigate(['/signup']);
}
toLogout(){
  this.Auth.logout();
}
  constructor(private router:Router, private Auth:AuthService) {
    
  } 

  ngOnInit() {
  }

}
