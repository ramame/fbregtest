import { Component, OnInit } from '@angular/core';
import { AuthService } from '../auth.service';
import { Router } from '@angular/router';


@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.css']
})
export class SignupComponent implements OnInit {
name = '';
email= '';
password= '';
code = 'notrealcode';
msg ='';
specialChar = ['!','@','#','$','%','^','&','*',
'(',')','_','-','+','=',"'",'"',
',','/','?','.','>','<','{','}',
'[',']','|','~','`',':',';'];

goodPas = false ;
showInculde = false
tempname = '';

  constructor(private auth:AuthService, private router:Router) { }
  
googleSign(){
this.auth.signupGoogle()
.then(value =>{
  this.auth.addUser(value.user,value.user.displayName,value.user.email);
  this.tempname = value.user.displayName;
}).then(value=> {this.router.navigate(['/welcome/'+this.tempname]);})
}

emailSign(){
this.specialChar.forEach(element => {
  if(this.password.includes(element)){
    this.goodPas=true;
  }
    
  });
  if(this.goodPas){
this.auth.signupEmail(this.email,this.password)
.then(value => {
  //לדוגמא כאן את יכולה להוסיף console.log(value)
  this.auth.updateName(value.user,this.name);
  this.auth.addUser(value.user,this.name,this.email); 
})
  .then(value => {
  this.router.navigate(['/welcome/'+this.name]);
})

.catch(value=>{
  console.log(value)
this.code=value.code;
this.msg = value.message;
})}
else{
  this.showInculde = true;
}

}


  ngOnInit() {
  }

}
