import { Component, OnInit, Input,Output,EventEmitter} from '@angular/core';
import { TodosService } from '../todos.service';

@Component({
  selector: 'app-todo',
  templateUrl: './todo.component.html',
  styleUrls: ['./todo.component.css']
})
export class TodoComponent implements OnInit {
  @Input() data:any;
  @Output() myButtonClicked = new EventEmitter<any>();
  show1;
  text:string;
  done:boolean;
  key:string;
  show2:boolean;
  todo:string;
  constructor(private TODOS:TodosService) { }

  checkboxclicked(){
  console.log(this.done)
  this.TODOS.update(this.key,this.text,this.done)
}

formshow(){
  this.todo = this.text;
  this.show2 = true;
}


cancel(){
  console.log(this.todo)
this.text = this.todo;
this.show2 = false;
}
saveTodo(){
  this.TODOS.update(this.key,this.text,this.done)
}
ke1y(){
  console.log(this.key)
}
show(){
  this.show1 =true;
}
notshow(){
  this.show1 =false;
}
delete(){
  console.log('ram')
  console.log(this.key);
this.TODOS.delete(this.key);
}
  ngOnInit() {
    this.show2 = false;
    this.show1 = false;
    this.key = this.data.$key;
    this.text = this.data.text;
    this.done = this.data.done;
  }

}
