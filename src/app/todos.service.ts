import { Injectable } from '@angular/core';
import { AngularFireDatabase, snapshotChanges } from '@angular/fire/database';
import { AuthService } from './auth.service';

@Injectable({
  providedIn: 'root'
})
export class TodosService {
addTodo(text){
  this.Auth.user.subscribe(value=>{
    let ref = this.db.database.ref('/');
    ref.child('users').child(value.uid).child('todos').push({'text':text, 'done':false}); 
  })
}

update(key,text,done){
  this.Auth.user.subscribe(user => {
  this.db.list('/users/'+user.uid+'/todos/').update(key, {'text':text,'done':done})
 //אני מעביר ל-אפדייט את הקיי ומתוך הקיי הזה איזה שדה לעדכן לשים לב לצורת הכתיבה

  })
 }
 
 delete(key){
  this.Auth.user.subscribe(user => {
    this.db.list('/users/'+user.uid+'/todos/').remove(key);
   //אני מעביר ל-אפדייט את הקיי ומתוך הקיי הזה איזה שדה לעדכן לשים לב לצורת הכתיבה
  
    })
 }
 //filterdb(){
  //this.Auth.user.subscribe(user => {
   // const todos = this.db.list('/users/'+user.uid+'/todos/').query.orderByChild('done').limitToFirst(5).on('value', snap=>{console.log(snap)});
   // console.log(todos)
//
   //אני מעביר ל-אפדייט את הקיי ומתוך הקיי הזה איזה שדה לעדכן לשים לב לצורת הכתיבה
  
 //   })
 
 //}
  constructor(private db:AngularFireDatabase, private Auth:AuthService) { }
}
