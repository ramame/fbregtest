import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { AuthService } from '../auth.service';
import { AngularFireDatabase } from '@angular/fire/database';
import { TodosService } from '../todos.service';

@Component({
  selector: 'app-welcome',
  templateUrl: './welcome.component.html',
  styleUrls: ['./welcome.component.css']
})
export class WelcomeComponent implements OnInit {
namefromurl='';
todos = [];
todo ='';
todosfilter = []
kindfilter;
filter = [
{text:'Done', value: '1'},
{text:'Not done', value: '2'},
{text:'Show all', value: '3'},
];

addTodo(){
  this.TODOS.addTodo(this.todo)
}
//filterdb(){
  //this.TODOS.filterdb();
//}

filterclient(){
  if (this.kindfilter == 1){
  this.todosfilter = this.todos.filter(value=>value.done==true)
  }
  if (this.kindfilter == 2){
  this.todosfilter = this.todos.filter(value=>value.done==false)
  }
  if (this.kindfilter == 3){
    this.todosfilter = this.todos
  }
}
  constructor(private route:ActivatedRoute, private Auth:AuthService, private db:AngularFireDatabase, private TODOS:TodosService) { }

  ngOnInit() {
    this.namefromurl = this.route.snapshot.paramMap.get('id');
    this.Auth.user.subscribe(user => {
      this.db.list('/users/'+user.uid+'/todos').snapshotChanges().subscribe(
        todos =>{                   //this.todos זה זה מלמעלה
                                         //סתם todos יהיה זה מתוך הדטה בייס
        this.todos = [];            //נאפס את כל הטודוס
        todos.forEach(                   
        todo => {                          // לכל אחד מהמשתנים במערך נקרא באופן זמני טודו
        let y = todo.payload.toJSON();     //y is a  temp variable,  we fill him (y) with payload us json.
                                             //payload came us string so we need change it to json
        y["$key"] = todo.key;              //אני מוסיף לכל טודו עוד תכונה שיקראו לה דולר קיי ובתוכה יהיה שם המפתח
                                           //key מילה שמורה
        this.todos.push(y);                //מכניס את האיבר שיצרתי בתוך טודוס שלנו
    this.todosfilter.push(y);
      }

      ) 
    }
    
    
    
        )  
    })
  
  }

}

  