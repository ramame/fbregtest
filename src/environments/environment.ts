// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase:{  apiKey: "AIzaSyDNztyhqWQjd5koQAmhOBd2lC2yvC_-DCQ",
  authDomain: "fbregtest-b7acd.firebaseapp.com",
  databaseURL: "https://fbregtest-b7acd.firebaseio.com",
  projectId: "fbregtest-b7acd",
  storageBucket: "fbregtest-b7acd.appspot.com",
  messagingSenderId: "673848126574"}
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
